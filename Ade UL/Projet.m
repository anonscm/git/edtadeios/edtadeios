//
//  Projet.m
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import "Projet.h"

@implementation Projet
@synthesize nomProjet, numeroProjet;


-(id)initWithNomProjet:(NSString *)_nomProjet numeroProjet:(NSInteger)_numeroProjet{
    if (self = [super init]) {
        nomProjet = _nomProjet;
        numeroProjet = _numeroProjet;
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.nomProjet forKey:kNomProjet];
    [aCoder encodeInteger:self.numeroProjet forKey:kNumeroProjet];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        //nomProjet = [[aDecoder decodeObjectForKey:kNomProjet]retain];
        nomProjet = [aDecoder decodeObjectForKey:kNomProjet];
        //numeroProjet = [[aDecoder decodeObjectForKey:kNumeroProjet]intValue];
        numeroProjet = [aDecoder decodeIntegerForKey:kNumeroProjet];
    }
    return self;
}


-(NSString *)description{
    return [NSString stringWithFormat:@"Projet de nom %@ et de numéro %d",nomProjet, numeroProjet];
}

@end
