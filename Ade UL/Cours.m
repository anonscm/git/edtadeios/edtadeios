//
//  Cours.m
//  Ade UL
//
//  Created by Didier Fradet on 07/11/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import "Cours.h"

@implementation Cours

@synthesize nom, note, dateString, heureDebut, heureFin;
@synthesize tabGroupes, tabProfs, tabSalles;
@synthesize coursId;
@synthesize absoluteSlot;
@synthesize datePresentation;

-(id)initWithNom:(NSString *)_nom dateString:(NSString *)_dateString heureDebut:(NSString *)_heureDebut heureFin:(NSString *)_heureFin note:(NSString *)_note tabGroupes:(NSArray *)_tabGroupes tabProfs:(NSArray *)_tabProfs tabSalles:(NSArray *)_tabSalles coursId:(NSString *)_coursId absoluteSlot:(NSString *)_absoluteSlot{
    if ([super init]) {
        nom = _nom;
        note = _note;
        dateString = _dateString;
        absoluteSlot = _absoluteSlot;
        heureDebut = _heureDebut;
        heureFin = _heureFin;
        tabGroupes = _tabGroupes;
        tabProfs = _tabProfs;
        tabSalles = _tabSalles;
        coursId = _coursId;
        
        NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
        NSDateFormatter* dfCours = [[NSDateFormatter alloc] init];
        [dfCours setDateFormat:kDateFormat];
        NSDateFormatter* df = [[NSDateFormatter alloc] init];
        [df setDateFormat:kDatePresentation];
        [df setLocale:frLocale];
        NSDate *date = [dfCours dateFromString:_dateString];
        datePresentation = [df stringFromDate:date];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:nom forKey:kNom];
    [aCoder encodeObject:dateString forKey:kDateString];
    [aCoder encodeObject:absoluteSlot forKey:kAbsoluteSlot];
    [aCoder encodeObject:heureDebut forKey:kHeureDebut];
    [aCoder encodeObject:heureFin forKey:kHeureFin];
    [aCoder encodeObject:note forKey:kNote];
    [aCoder encodeObject:tabGroupes forKey:kTabGroupes];
    [aCoder encodeObject:tabProfs forKey:kTabProfs];
    [aCoder encodeObject:tabSalles forKey:kTabSalles];
    [aCoder encodeObject:coursId forKey:kCoursId];
    [aCoder encodeObject:datePresentation forKey:kDatePresentation];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        nom = [aDecoder decodeObjectForKey:kNom];
        note = [aDecoder decodeObjectForKey:kNote];
        dateString = [aDecoder decodeObjectForKey:kDateString];
        absoluteSlot = [aDecoder decodeObjectForKey:kAbsoluteSlot];
        heureDebut = [aDecoder decodeObjectForKey:kHeureDebut];
        heureFin = [aDecoder decodeObjectForKey:kHeureFin];
        tabGroupes = [aDecoder decodeObjectForKey:kTabGroupes];
        tabProfs = [aDecoder decodeObjectForKey:kTabProfs];
        tabSalles = [aDecoder decodeObjectForKey:kTabSalles];
        coursId = [aDecoder decodeObjectForKey:kCoursId];
        datePresentation = [aDecoder decodeObjectForKey:kDatePresentation];
    }
    return self;
}


@end
