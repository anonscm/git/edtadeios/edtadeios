//
//  DetailViewController.m
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import "DetailViewController.h"
#import "Cours.h"
#import "DetailCoursControllerViewController.h"

@interface DetailViewController(){
    NSArray *sortedKeys;
}

@end

@implementation DetailViewController

@synthesize dictCours;

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *keys = [dictCours allKeys];
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    [df setDateFormat:kDatePresentation];
    [df setLocale:frLocale];
    sortedKeys = [keys sortedArrayUsingComparator:^(NSString *obj1, NSString *obj2) {
        NSDate *date1 = [df dateFromString:obj1];
        //NSLog(@"Objet 1 : %@, Date 1 : %@",obj1,date1);
        NSDate *date2 = [df dateFromString:obj2];
        return [date1 compare:date2];
    }];
    	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //NSLog(@"Nombre de sections %d",[[dictCours allKeys] count]);
    return [[dictCours allKeys] count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //NSString *key = [[dictCours allKeys]objectAtIndex:section];
    NSString *key = [sortedKeys objectAtIndex:section];
    return [[dictCours valueForKey:key]count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
   return [sortedKeys objectAtIndex:section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellCours"];
    //NSString *key = [[dictCours allKeys]objectAtIndex:indexPath.section];
    NSString *key = [sortedKeys objectAtIndex:indexPath.section];
    NSArray *tabCours = [dictCours valueForKey:key];
    Cours *cours = [tabCours objectAtIndex:indexPath.row];
    cell.textLabel.text = cours.nom;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@",cours.heureDebut, cours.heureFin];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetailCourse"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *key = [sortedKeys objectAtIndex:indexPath.section];
        NSArray *tabCours = [dictCours valueForKey:key];
        Cours *coursChoisi = [tabCours objectAtIndex:indexPath.row];
        DetailCoursControllerViewController *detail = segue.destinationViewController;
        detail.cours = coursChoisi;
    }
}

@end
