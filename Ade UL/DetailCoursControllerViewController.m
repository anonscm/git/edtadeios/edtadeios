//
//  DetailCoursControllerViewController.m
//  Ade UL
//
//  Created by Didier Fradet on 08/11/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import "DetailCoursControllerViewController.h"
#import "Cours.h"

@implementation DetailCoursControllerViewController

@synthesize cours;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = cours.nom;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int retour = 0;
    switch (section) {
        /*case 0:
            retour = 1;
            break;*/
        case 0:
            retour = 1;
            break;
        case 1:
            retour = MAX(cours.tabProfs.count, 1);
            break;
        case 2:
            retour = MAX(cours.tabSalles.count,1);
            break;
        case 3:
            retour = MAX(cours.tabGroupes.count,1);
            break;
        case 4:
            retour = 1;
            break;
        default:
            retour = 1;
            break;
    }
    
    return retour;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *retour;
    switch (section) {
        /*case 0:
            retour = kSectionNom;
            break;*/
        case 0:
            retour = kSectionHoraire;
            break;
        case 1:
            retour = kSectionProfs;
            break;
        case 2:
            retour = kSectionSalles;
            break;
        case 3:
            retour = kSectionGroupes;
            break;
        case 4:
            retour = kSectionNote;
            break;
        default:
            retour = @"";
            break;
    }
    
    return retour;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"CellCours";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    switch (indexPath.section) {
        /*case 0:
            cell.textLabel.text = cours.nom;
            break;*/
        case 0:
            cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@",cours.heureDebut, cours.heureFin];
            break;
        case 1:
            if (cours.tabProfs.count>0) {
                cell.textLabel.text = [cours.tabProfs objectAtIndex:indexPath.row];
            }else{
                cell.textLabel.text = @"";
            }
            break;
        case 2:
            if (cours.tabSalles.count>0) {
                cell.textLabel.text = [cours.tabSalles objectAtIndex:indexPath.row];
            }else{
                cell.textLabel.text = @"";
            }
            break;
        case 3:
            if (cours.tabGroupes.count>0) {
                cell.textLabel.text = [cours.tabGroupes objectAtIndex:indexPath.row];
            }else{
                cell.textLabel.text = @"";
            }
            break;
        case 4:
            cell.textLabel.text = cours.note;
            break;
        default:
            cell.textLabel.text = @"";
            break;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
