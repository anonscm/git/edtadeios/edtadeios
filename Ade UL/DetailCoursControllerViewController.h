//
//  DetailCoursControllerViewController.h
//  Ade UL
//
//  Created by Didier Fradet on 08/11/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Cours;

#define kSectionNom @"Nom du cours"
#define kSectionHoraire @"Horaire"
#define kSectionProfs @"Enseignant(s)"
#define kSectionSalles @"Salle(s)"
#define kSectionGroupes @"Groupe(s) d'étudiants"
#define kSectionNote @"Note de séance"

@interface DetailCoursControllerViewController : UITableViewController

@property (nonatomic, strong) Cours *cours;

@end
