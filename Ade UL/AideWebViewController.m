//
//  AideWebViewController.m
//  Ade UL
//
//  Created by Didier Fradet on 28/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import "AideWebViewController.h"

@interface AideWebViewController ()

@end

@implementation AideWebViewController
@synthesize webView;

/*- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://ios-test.uhp-nancy.fr/ade/ressource.html"]];
    [webView loadRequest:urlRequest];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setWebView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
