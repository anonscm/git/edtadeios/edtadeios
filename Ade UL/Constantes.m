//
//  Constantes.m
//  Ade UL
//
//  Created by Didier Fradet on 04/11/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import "Constantes.h"

@implementation Constantes

NSString* const kWebApiAde=@"http://****/jsp/webapi?";
NSString* const kLoginAde=@"****";
NSString* const kPasswordAde=@"****";
NSString* const kFormatDate=@"MM/dd/yyyy";
NSInteger const kInterval = 10;
NSInteger const kUpdate = 300;
NSString* const kBackground = @"backGround";
@end
