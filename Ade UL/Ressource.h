//
//  Ressource.h
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Projet.h"

#define kNomRessource @"nomRessource"
#define kProjet @"projet"
#define kNumeroRessource @"numeroRessource"
#define kUpdated @"updated"

@interface Ressource : NSObject<NSCoding>{
    @private
    NSString *nomRessource;
    Projet *projet;
    NSInteger numeroRessource;
    BOOL updated;
}



@property(strong, nonatomic) NSString *nomRessource;
@property(strong, nonatomic) Projet *projet;
@property(assign) NSInteger numeroRessource;
@property(assign)BOOL updated;

-(id)initWithNomRessource:(NSString*) _nomRessource numeroRessource:(NSInteger)_numeroRessource projet:(Projet*)_projet;
-(NSString*) descriptionNumeroProjet;

@end
