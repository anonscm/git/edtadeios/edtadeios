//
//  Projet.h
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kNomProjet @"nomProjet"
#define kNumeroProjet @"numeroProjet"

@interface Projet : NSObject<NSCoding>{
    @private
    NSString *nomProjet;
    NSInteger numeroProjet;
}
@property(nonatomic, strong) NSString *nomProjet;
@property(assign) NSInteger numeroProjet;

-(id)initWithNomProjet:(NSString *)_nomProjet numeroProjet:(NSInteger)_numeroProjet;
@end
