//
//  Cours.h
//  Ade UL
//
//  Created by Didier Fradet on 07/11/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kNom @"nom"
#define kCoursId @"coursId"
#define kDateString @"dateString"
#define kAbsoluteSlot @"absoluteSlot"
#define kDateFormat @"dd/MM/yyyy"
#define kDatePresentation @"EEEE dd MMMM yyyy"
#define kHeureDebut @"heureDebut"
#define kHeureFin @"heureFin"
#define kNote @"note"
#define kTabGroupes @"tabGroupes"
#define kTabSalles @"tabSalles"
#define kTabProfs @"tabProfs"

@interface Cours : NSObject<NSCoding>

@property(nonatomic,strong) NSString *nom;
@property(nonatomic,strong) NSString *coursId;
@property(nonatomic,strong) NSString *dateString;
@property(nonatomic,strong) NSString *datePresentation;
@property(nonatomic,strong) NSString *absoluteSlot;
@property(nonatomic,strong) NSString *heureDebut;
@property(nonatomic,strong) NSString *heureFin;
@property(nonatomic,strong) NSString *note;
@property(nonatomic,strong) NSArray *tabGroupes;
@property(nonatomic,strong) NSArray *tabSalles;
@property(nonatomic,strong) NSArray *tabProfs;

-(id) initWithNom:(NSString*) _nom dateString:(NSString*)_dateString heureDebut :(NSString*) _heureDebut heureFin:(NSString*) _heureFin note:(NSString*) _note tabGroupes :(NSArray*) _tabGroupes tabProfs:(NSArray*) _tabProfs tabSalles:(NSArray*) _tabSalles coursId:(NSString*) _coursId absoluteSlot:(NSString*)_absoluteSlot;

@end
