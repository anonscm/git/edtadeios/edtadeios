//
//  AideWebViewController.h
//  Ade UL
//
//  Created by Didier Fradet on 28/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AideWebViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
