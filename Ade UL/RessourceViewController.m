//
//  RessourceViewController.m
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import "RessourceViewController.h"
#import "Constantes.h"
#import "TBXML.h"

/*@interface RessourceViewController ()

@end*/

@implementation RessourceViewController
@synthesize nomRessource;
@synthesize numeroRessource;
@synthesize pickerView;
@synthesize ressource, delegate, index;
@synthesize tabProjets;
@synthesize projChoisi;

/*- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}*/



- (void)viewDidLoad
{
    [super viewDidLoad];
    nomRessource.delegate = self;
    numeroRessource.delegate = self;
    nomRessource.text = ressource.nomRessource;
    numeroRessource.text = [NSString stringWithFormat:@"%d",ressource.numeroRessource];
    self.tabProjets = [NSMutableArray array];
    //[tabProjets addObject:[[Projet alloc]initWithNomProjet:@"" numeroProjet:0]];
    
    NSError *error;
    
    NSString *urlString = [NSString stringWithFormat:@"%@function=connect&login=%@&password=%@", kWebApiAde, kLoginAde, kPasswordAde];
    // Connexion récupération de sessionId
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *xmlData = [NSData dataWithContentsOfURL:url];
    TBXML *tbxml = [TBXML newTBXMLWithXMLData:xmlData error:&error];
    TBXMLElement *root = [tbxml rootXMLElement];
    NSString *sessionId = [TBXML valueOfAttributeNamed:@"id" forElement:root error:&error];
    
    //liste des projets
    urlString = [NSString stringWithFormat:@"%@sessionId=%@&function=getProjects&detail=2", kWebApiAde, sessionId];
    url = [NSURL URLWithString:urlString];
    xmlData = [NSData dataWithContentsOfURL:url];
    tbxml = [TBXML newTBXMLWithXMLData:xmlData error:&error];
    root = [tbxml rootXMLElement];
    TBXMLElement *element = [TBXML childElementNamed:@"project" parentElement:root error:&error];
    while (element!=nil) {
        Projet *projet = [[Projet alloc]initWithNomProjet:[TBXML valueOfAttributeNamed:@"name" forElement:element error:&error]
                                              numeroProjet:[[TBXML valueOfAttributeNamed:@"id" forElement:element error:&error]intValue]];
        //NSLog(@"Nom : %@, numéro : %@", [TBXML valueOfAttributeNamed:@"name" forElement:element error:&error], [TBXML valueOfAttributeNamed:@"id" forElement:element error:&error]);
        [tabProjets addObject:projet];
        element = [TBXML nextSiblingNamed:@"project" searchFromElement:element error:&error];
    }
    //Déconnexion
    urlString = [NSString stringWithFormat:@"%@sessionId=%@&function=disconnect", kWebApiAde, sessionId];
    url = [NSURL URLWithString:urlString];
    xmlData = [NSData dataWithContentsOfURL:url];
    tbxml = [TBXML newTBXMLWithXMLData:xmlData error:&error];
    root = [tbxml rootXMLElement];
    
	// Do any additional setup after loading the view.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.nomRessource) {
        [textField resignFirstResponder];
    }else if (textField == self.numeroRessource) {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)viewDidUnload
{
    [self setNomRessource:nil];
    [self setNumeroRessource:nil];
    [self setRessource:nil];
    [self setPickerView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)modifierRessource:(id)sender {
    ressource.projet = projChoisi;
    ressource.nomRessource = nomRessource.text;
    ressource.numeroRessource = [numeroRessource.text intValue];
    ressource.updated = YES;
   [self.delegate ressourceControllerDidsave:self updateRessource:ressource atIndex:index];
    //NSLog(@"Nom ressource : %@ et numéro ressource %d",ressource.nomRessource, ressource.numeroRessource);
    //NSLog(@"Index : %d", index);

}


#pragma mark -
#pragma mark PickerView DataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return tabProjets.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [[tabProjets objectAtIndex:row]nomProjet];
}

#pragma mark -
#pragma mark PickerView Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    projChoisi = [tabProjets objectAtIndex:row];
}

@end
