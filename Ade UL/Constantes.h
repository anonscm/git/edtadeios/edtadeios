//
//  Constantes.h
//  Ade UL
//
//  Created by Didier Fradet on 04/11/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constantes : NSObject

extern NSString* const kWebApiAde;
extern NSString* const kLoginAde;
extern NSString* const kPasswordAde;
extern NSString* const kFormatDate;
extern NSInteger const kInterval;
extern NSInteger const kUpdate;
extern NSString* const kBackground;

@end
