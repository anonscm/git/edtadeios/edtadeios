//
//  Ressource.m
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import "Ressource.h"

@implementation Ressource

@synthesize nomRessource, numeroRessource, projet, updated;

-(id)initWithNomRessource:(NSString *)_nomRessource numeroRessource:(NSInteger)_numeroRessource projet:(Projet *)_projet{
    if (self==[super init]) {
        nomRessource = _nomRessource;
        numeroRessource = _numeroRessource;
        projet = _projet;
        updated = NO;
    } 
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:nomRessource forKey:kNomRessource];
    [aCoder encodeObject:projet forKey:kProjet];
    //[aCoder encodeObject:[NSNumber numberWithInt:self.numeroRessource] forKey:kNumeroRessource];
    [aCoder encodeInteger:numeroRessource forKey:kNumeroRessource];
    [aCoder encodeBool:updated forKey:kUpdated];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        nomRessource = [aDecoder decodeObjectForKey:kNomRessource];
        projet = [aDecoder decodeObjectForKey:kProjet];
        //numeroRessource = [[aDecoder decodeObjectForKey:kNumeroRessource]intValue];
        numeroRessource = [aDecoder decodeIntegerForKey:kNumeroRessource];
        updated = [aDecoder decodeBoolForKey:kUpdated];
    }
    return self;
}



-(NSString *)description{
    return [NSString stringWithFormat:@"Ressource de nom %@, de numéro %d, de numéro de projet %d",nomRessource, numeroRessource, projet.numeroProjet];
}

-(NSString *)descriptionNumeroProjet{
    return [NSString stringWithFormat:@"Projet : %@, numéro : %d",projet.nomProjet, numeroRessource];
}


@end
