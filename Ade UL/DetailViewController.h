//
//  DetailViewController.h
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource>

@property(nonatomic, strong) NSDictionary *dictCours;


@end
