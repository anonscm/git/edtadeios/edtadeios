//
//  MasterViewController.m
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"
#import "RessourceViewController.h"
#import "Ressource.h"
#import "Projet.h"
#import "Constantes.h"
#import "TBXML.h"
#import "Cours.h"


@implementation MasterViewController
@synthesize tabRessources;
@synthesize dateJourString, dateFinaleString;
@synthesize background;


- (void)awakeFromNib
{
    [super awakeFromNib];
}

-(void) changeColorButton{
    self.background = NO;
    [self.tableView reloadData];
}


-(void)enregistrerSurDisque{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"resources.plist"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:tabRessources];
    [data writeToFile:path atomically:YES];
}

-(void)ressourceControllerDidsave:(RessourceViewController *)controller updateRessource:(Ressource *)ressource atIndex:(NSInteger)index{
    [tabRessources replaceObjectAtIndex:index withObject:ressource];
    [self enregistrerSurDisque];
    self.background = YES;
    [self.tableView reloadData];
    /*for (Ressource *res in tabRessources) {
        NSLog(@"Nom ressource : %@ et numéro ressource %d",res.nomRessource, res.numeroRessource);
    }*/
//    NSLog(@"Nom ressource : %@ et numéro ressource %d",ressource.nomRessource, ressource.numeroRessource);
//    NSLog(@"Index : %d", index);
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    RessourceViewController *resViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ressourceViewController"];
    Ressource * ressource = [tabRessources objectAtIndex:indexPath.row];
    //NSLog(@"%@",ressource.nomRessource);
    resViewController.ressource = ressource;
    resViewController.index = indexPath.row;
    resViewController.delegate = self;
    [self.navigationController pushViewController:resViewController animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"resources.plist"];
    //NSLog(@"chemin : %@",path);
    self.tabRessources = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    
    if (!tabRessources) {
        tabRessources = [[NSMutableArray alloc] init];
    }
    
    // Do any additional setup after loading the view, typically from a nib.
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    //self.editButtonItem.title = @"EDITER";
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(changeColorButton) name:kBackground object:nil];
    //NSLog(@"Inscrit au centre de notification");
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)insertNewObject:(id)sender
{
    if (!tabRessources) {
        tabRessources = [[NSMutableArray alloc] init];
    }
    Projet *p = [[Projet alloc]initWithNomProjet:@"Projet 1" numeroProjet:1];
    Ressource *r = [[Ressource alloc]initWithNomRessource:@"A modifier ->" numeroRessource:10 projet:p];
    [tabRessources insertObject:r atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"Supprimer";
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tabRessources.count;
}

-(void) update:(id) sender{
    
    NSDate *dateJour = [NSDate date];
    NSDateFormatter *nsDateFormat = [[NSDateFormatter alloc]init];
    [nsDateFormat setDateFormat:kFormatDate];
    self.dateJourString = [nsDateFormat stringFromDate:dateJour];
    //NSLog(@"%@", dateJourString);
    NSTimeInterval duree = kInterval*24*60*60;
    NSDate *dateFinale = [dateJour dateByAddingTimeInterval:duree];
    self.dateFinaleString = [nsDateFormat stringFromDate:dateFinale];
    //NSLog(@"%@", dateFinaleString);

    
    UITableViewCell *cell = (UITableViewCell*)[sender superview];
    NSIndexPath *path = [self.tableView indexPathForCell:cell];
    Ressource *resChoisi = [tabRessources objectAtIndex:path.row];
    
    //NSLog(@"%@, numProjet %d, numRessource %d", resChoisi.nomRessource, resChoisi.projet.numeroProjet, resChoisi.numeroRessource);
    
    NSError *error;
    
    NSMutableArray *tabCours = [NSMutableArray array];
    NSSortDescriptor *absoluteSlotSorter = [[NSSortDescriptor alloc] initWithKey:@"absoluteSlot" ascending:YES];
    NSString *urlString = [NSString stringWithFormat:@"%@function=connect&login=%@&password=%@", kWebApiAde, kLoginAde, kPasswordAde];
    
    // Connexion récupération de sessionId
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *xmlData = [NSData dataWithContentsOfURL:url];
    TBXML *tbxml = [TBXML newTBXMLWithXMLData:xmlData error:&error];
    TBXMLElement *root = [tbxml rootXMLElement];
    NSString *sessionId = [TBXML valueOfAttributeNamed:@"id" forElement:root error:&error];
    //NSLog(@"Session id : %@",sessionId);
    
    //Connexion à un projet
    urlString = [NSString stringWithFormat:@"%@sessionId=%@&function=setProject&projectId=%d", kWebApiAde, sessionId, resChoisi.projet.numeroProjet];
    url = [NSURL URLWithString:urlString];
    xmlData = [NSData dataWithContentsOfURL:url];
    tbxml = [TBXML newTBXMLWithXMLData:xmlData error:&error];
    root = [tbxml rootXMLElement];
    
    //Récupération des cours
    urlString = [NSString stringWithFormat:@"%@sessionId=%@&function=getEvents&resources=%d&startDate=%@&endDate=%@&detail=8",
                 kWebApiAde, sessionId, resChoisi.numeroRessource, dateJourString, dateFinaleString];
    url = [NSURL URLWithString:urlString];
    xmlData = [NSData dataWithContentsOfURL:url];
    tbxml = [TBXML newTBXMLWithXMLData:xmlData error:&error];
    root = [tbxml rootXMLElement];
    TBXMLElement *event = [TBXML childElementNamed:@"event" parentElement:root error:&error];
    while (event !=nil) {
        NSString *res;
        NSMutableArray *tabG = [NSMutableArray array];
        NSMutableArray *tabP = [NSMutableArray array];
        NSMutableArray *tabS = [NSMutableArray array];
        
        NSString *eventId = [NSString stringWithFormat:@"%d-%@", resChoisi.projet.numeroProjet,
                             [TBXML valueOfAttributeNamed:@"id" forElement:event error:&error]];
        //NSLog(@"Id de l'event : %@",id);
        NSString *nom = [TBXML valueOfAttributeNamed:@"name" forElement:event error:&error];
        NSString *note = [TBXML valueOfAttributeNamed:@"note" forElement:event error:&error];
        NSString *heureDebut = [TBXML valueOfAttributeNamed:@"startHour" forElement:event error:&error];
        NSString *heureFin = [TBXML valueOfAttributeNamed:@"endHour" forElement:event error:&error];
        NSString *dateString = [TBXML valueOfAttributeNamed:@"date" forElement:event error:&error];
        NSString *absoluteSlot = [TBXML valueOfAttributeNamed:@"absoluteSlot" forElement:event error:&error];
        
        TBXMLElement *resources = [TBXML childElementNamed:@"resources" parentElement:event error:&error];
        while (resources!=nil) {
            TBXMLElement *resource = [TBXML childElementNamed:@"resource" parentElement:resources error:&error];
            while (resource!=nil) {
                res = [TBXML valueOfAttributeNamed:@"category" forElement:resource];
                if([res isEqualToString:@"trainee"]){
                    [tabG addObject:[TBXML valueOfAttributeNamed:@"name" forElement:resource error:&error]];
                }else if([res isEqualToString:@"instructor"]){
                    [tabP addObject:[TBXML valueOfAttributeNamed:@"name" forElement:resource error:&error]];
                }else if([res isEqualToString:@"classroom"]){
                    [tabS addObject:[TBXML valueOfAttributeNamed:@"name" forElement:resource error:&error]];
                }
                resource = [TBXML nextSiblingNamed:@"resource" searchFromElement:resource error:&error];
            }
            resources = [TBXML nextSiblingNamed:@"resources" searchFromElement:resources error:&error];
        }
        Cours *unCours = [[Cours alloc]initWithNom:nom dateString:dateString heureDebut:heureDebut heureFin:heureFin note:note tabGroupes:tabG tabProfs:tabP tabSalles:tabS coursId:eventId absoluteSlot:absoluteSlot];
        //[listeCours setObject:unCours forKey:eventId];
        [tabCours addObject:unCours];
                
        event = [TBXML nextSiblingNamed:@"event" searchFromElement:event error:&error];
    }
    //Tri du tableau suivant absolutSlot
    NSArray *tab = [tabCours sortedArrayUsingDescriptors:[NSArray arrayWithObject:absoluteSlotSorter]];
    
    //Création du dictionnaire
   
    NSMutableDictionary *dictCours = [NSMutableDictionary dictionary];
    for (Cours *c in tab) {
        NSMutableArray* coursParDate = [dictCours objectForKey:c.datePresentation];
        if (coursParDate == nil) {
            coursParDate = [NSMutableArray array];
            [dictCours setObject:coursParDate forKey:c.datePresentation];
        }
        [coursParDate addObject:c];
    }
          
    //Déconnexion
    urlString = [NSString stringWithFormat:@"%@sessionId=%@&function=disconnect", kWebApiAde, sessionId];
    url = [NSURL URLWithString:urlString];
    xmlData = [NSData dataWithContentsOfURL:url];
    tbxml = [TBXML newTBXMLWithXMLData:xmlData error:&error];
    root = [tbxml rootXMLElement];
    /*NSLog(@"Taille liste de cours : %d", tab.count);
    for (Cours *c in tab) {
        NSLog(@"Cours ID  : %@",c.coursId);
        NSLog(@"Nom : %@", c.nom);
        NSLog(@"Note : %@", c.note);
        NSLog(@"Date : %@", c.dateString);
        NSLog(@"Heure début : %@", c.heureDebut);
        NSLog(@"Heure fin : %@", c.heureFin);
        NSLog(@"Profs : %@", c.tabProfs);
        NSLog(@"Groupes : %@", c.tabGroupes);
        NSLog(@"Salles : %@", c.tabSalles);
        NSLog(@"");
    }*/
    
    //Archivage dans le fichier .plist
    NSString *nomFichier = [NSString stringWithFormat:@"%d-%d.plist", resChoisi.projet.numeroProjet, resChoisi.numeroRessource];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pathDirectory = [documentsDirectory stringByAppendingPathComponent:nomFichier];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[NSDictionary dictionaryWithDictionary:dictCours]];
    [data writeToFile:pathDirectory atomically:YES];
    
   [sender setImage:[UIImage imageNamed:@"status_updated"] forState:UIControlStateNormal];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    Ressource *ressource = [tabRessources objectAtIndex:indexPath.row];
    
    UIButton *bouton = [[UIButton alloc]initWithFrame:CGRectMake(5, 5, 40, 40)];
        
    if (self.background == NO) {
       [bouton setImage:[UIImage imageNamed:@"status_waiting_for_update"] forState:UIControlStateNormal];
    }
    
    if (ressource.updated) {
        
        if (self.background == NO) {
            [bouton setImage:[UIImage imageNamed:@"status_waiting_for_update"] forState:UIControlStateNormal];
        }
        //cell.imageView.image = [UIImage imageNamed:@"status_waiting_for_update"];
    }else {
        
        [bouton setImage:[UIImage imageNamed:@"status_not_updated"] forState:UIControlStateNormal];
        
        //[bouton setImage:[UIImage imageNamed:@"status_not_updated"] forState:UIControlStateNormal];
        //cell.imageView.image = [UIImage imageNamed:@"status_not_updated.png"];
    }
    
        
    [bouton addTarget:self action:@selector(update:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell addSubview:bouton];
    [cell setIndentationWidth:45];
    [cell setIndentationLevel:1];
    
    cell.textLabel.text = ressource.nomRessource;
    cell.detailTextLabel.text = [ressource descriptionNumeroProjet];
    
       
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*if (tableView.editing) {
        self.editButtonItem.title =  @"CustomDoneName";
    }
    else{
         self.editButtonItem.title = @"EDITER";
    }*/
       
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Ressource *res = [tabRessources objectAtIndex:indexPath.row];
        NSString *nomFichier = [NSString stringWithFormat:@"%d-%d.plist", res.projet.numeroProjet, res.numeroRessource];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:nomFichier];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        [fileManager removeItemAtPath:path error:&error];
        
        [tabRessources removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self enregistrerSurDisque];
        
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showCourses"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Ressource *res = [tabRessources objectAtIndex:indexPath.row];
        //NSLog(@"Nom ressource : %@", res.nomRessource);
        
        NSString *nomFichier = [NSString stringWithFormat:@"%d-%d.plist", res.projet.numeroProjet, res.numeroRessource];
        
        //désarchivage du fichier .plist
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:nomFichier];
        //NSLog(@"Chemin : %@", path);
        NSDictionary *dict  = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        //NSLog(@"Taille dictionnaire %d",dict.count);
        DetailViewController *detail = segue.destinationViewController;
        detail.dictCours = dict;
    }
}

@end

