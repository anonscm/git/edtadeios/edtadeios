//
//  RessourceViewController.h
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ressource.h"
#import "Projet.h"

@class RessourceViewController;
@class Ressource;
@protocol RessourceViewControllerDelegate <NSObject>

-(void) ressourceControllerDidsave : (RessourceViewController *) controller updateRessource:(Ressource *) ressource atIndex:(NSInteger) index;

@end


@interface RessourceViewController : UIViewController<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *nomRessource;
@property (strong, nonatomic) IBOutlet UITextField *numeroRessource;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) NSMutableArray *tabProjets;
@property (strong, nonatomic) Ressource *ressource;
@property (strong, nonatomic) id<RessourceViewControllerDelegate> delegate;
@property (assign) NSInteger index;
@property (strong, nonatomic) Projet *projChoisi;

- (IBAction)modifierRessource:(id)sender;

@end
