//
//  MasterViewController.h
//  Ade UL
//
//  Created by Didier Fradet on 20/06/12.
//  Copyright (c) 2012 Université Henri Poincaré. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RessourceViewController.h"
#import "AppDelegate.h"

@interface MasterViewController : UITableViewController<RessourceViewControllerDelegate>

@property(strong, nonatomic) NSMutableArray *tabRessources;
@property(strong, nonatomic) NSString *dateJourString;
@property(strong, nonatomic) NSString *dateFinaleString;
@property(assign, nonatomic) BOOL background;


@end
